﻿// Хранимые настройки логирования
//  

#Область ПрограммныйИнтерфейс

// Сохранение настроек. По-умолчанию применяются настройки региона,
// 	возможно уточнение для конкретного пользователя
//
// Параметры: 
// 	Настройки - Структура - Настройки логирования. Полные настройки региона вида
// 		Элементы структуры
// 		Включен - флаг активности логирования для региона
// 		Уровень - Уровень логирования для региона
// 		Шаблон - Строка шаблона записи лога для региона, см. Шаблон()
// 		Пользователи - Соответствие с настройками по пользователям
// 			Ключ - идентификатор пользователя
// 			Значение - Структура настроек
// 	Регион - Строка - Регион использования адаптеров
//
Процедура Сохранить(Настройки, Регион = Неопределено) Экспорт
	
	лг_НастройкаВызовСервера.Сохранить(Настройки, Регион);
	
КонецПроцедуры // Сохранить 

// Возвращает хранимые настройки
//
// Параметры: 	
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Структура
//
Функция Получить(Регион = Неопределено) Экспорт
	
	Возврат Новый Структура(лг_НастройкаПовтИсп.Получить(Регион));
	
КонецФункции // Получить 

// Возвращает настройку по-умолчанию
//
// Параметры: 
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Структура
//
Функция Умолчание(Регион = Неопределено) Экспорт
	
	Возврат Новый Структура(лг_НастройкаПовтИсп.Умолчание(Регион));
	
КонецФункции // Умолчание 

// Возвращает коллекцию настроек региона для всех пользователей
//
// Параметры: 
// 	Регион - Строка - Регион использования
//
// Возвращаемое значение: 
// 	Соответствие 
//
Функция Коллекция(Регион = Неопределено) Экспорт
	
	Результат	= лг_НастройкаПовтИсп.Коллекция(Регион);
	
	Возврат ?(Результат = Неопределено
	, Результат
	, лг_Модуль.ом_Коллекция().ФиксированнаяВОбычная(Результат));
	
КонецФункции // Коллекция 

//
// Свойства настроек
// 

// Возвращает уровень логирования текущего пользователя
//
// Параметры: 
// 	Настройка - Структура - Состав настроек
// 	Значение - Булево - Новое значение
//
// Возвращаемое значение: 
// 	Булево
//
Функция Уровень(Настройка, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Настройка, "Уровень", Значение);
	
КонецФункции // Уровень 

// Возвращает флаг активности логирования текущего пользователя
//
// Параметры: 
// 	Настройка - Структура - Состав настроек
// 	Значение - Булево - Новое значение
//
// Возвращаемое значение: 
// 	Булево
//
Функция Включен(Настройка, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Настройка, "Включен", Значение);
	
КонецФункции // Включен 

// Возвращает шаблон записи лога
//
// Параметры: 
// 	Настройка - Структура - Состав настроек
// 	Значение - Строка - Новое значение
//
// Возвращаемое значение: 
// 	Строка
//
Функция Шаблон(Настройка, Значение = Null) Экспорт
	
	Возврат лг_Модуль.ом_Коллекция().СтруктураСвойство(Настройка, "Шаблон", Значение);
	
КонецФункции // Шаблон 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
