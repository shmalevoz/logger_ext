﻿// Поставщик данных записей логов
//  
// BSLLS:NonStandardRegion-off - ложное срабатывание

#Область ОбработчикиСобытийФормы

// Предопределенный метод
// 
// BSLLS:MissingParameterDescription-off - см. синтакс-помощник
&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	// BSLLS:MissingParameterDescription-on 
	
	Отказ		= Истина;
	Сообщение	= Новый СообщениеПользователю;
	Сообщение.Текст = лг_ПодсистемаТексты.ФормаСлужебнаяСообщение();
	Сообщение.Сообщить(); 
	
КонецПроцедуры

#КонецОбласти

#Область ПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти

// BSLLS:NonStandardRegion-on
