﻿// Поставщик данных логирования
//  

#Область ПрограммныйИнтерфейс

// Возвращает возможные данные шаблона записи лога
//
// Параметры: 
// 	Логгер - Структура - Настройка логгера
// 	СредаВыполнения - Число - Среда выполнения
//
// Возвращаемое значение: 
// 	Структура 
//
Функция Данные(Логгер, СредаВыполнения) Экспорт
	
	Возврат Новый Структура();
	
КонецФункции // Данные 

// Возвращает массив доступных элементов шаблонов лога
//
// Параметры: 
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Структура
//
Функция Элементы(Регион = Неопределено) Экспорт
	
	Возврат Новый Структура();
	
КонецФункции // Элементы 

// Возвращает шаблон по-умолчанию записи лога
//
// Параметры: 
// 	Регион - Строка - Регион использования адаптеров
//
// Возвращаемое значение: 
// 	Строка
//
Функция Умолчание(Регион = Неопределено) Экспорт
	
	Возврат "";
	
КонецФункции // Умолчание 

// 
// Общий интерфейс адаптеров
// 

// Возвращает параметры адаптера по-умолчанию
// 
// Параметры: 
// 
// Возвращаемое значение: 
// 	Соответствие
// 
Функция ПараметрыУмолчание() Экспорт
	
	Результат	= Новый Соответствие();
	
	Возврат Результат;
	
КонецФункции // ПараметрыУмолчание 

// Инициализация адаптера
// 
// Параметры: 
// 	Регион - Строка - Регион использования адаптеров
// 	Параметры - Соответствие - Сохраненные параметры
// 
Процедура Инициализировать(Регион, Параметры) Экспорт
	
КонецПроцедуры // Инициализировать 

#КонецОбласти

#Область СлужебныйПрограммныйИнтерфейс

#КонецОбласти

#Область СлужебныеПроцедурыИФункции

#КонецОбласти
